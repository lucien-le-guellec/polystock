# PolyStock

## Contexte

Dans le cadre du projet de Conduite de test, nous avons réalisé cette application web basée sur une API REST en s'inspirant de l'existant créé par Florent Clarret [ici](https://github.com/FlorentClarret/game-api).

## La base de données

### Installation

Installez PostgreSQL 11.06.
(Optionnel) Installez PGAdmin4 v4.16.

Créez un serveur sur PGAdmin4 (localhost par exemple) puis créez deux base de données : DEV et dbunit.

Avec l'utilisateur postgres (créé automatiquement sur Ubuntu avec l'installation de PostgreSQL) lancez les commandes suivantes pour alimenter les base de données depuis le dump :

```
psql DEV < [chemin_vers_le_dump]/polystock_dump.sql
psql dbunit < [chemin_vers_le_dump]/polystock_dump.sql
```

Toujours avec l'utilisateur postgres, créez un utilisateur "dbunit" et donnez lui tous les privilèges.

A noter que les tests DBUnit nécessitent qu'au moins la base de données "dbunit" soit créée.

## L'application

### Description

Cette application est basée sur une API REST pour gérer des magasins, des rayons au sein des magasins, et des articles au sein des rayons.

### Lancement du service

Placez vous à la racine du dossier vtock-api, et utilisez la commande suivante :

```
 $ mvn spring-boot:run
```

Vous pouvez accéder à l'API REST à l'aide d'une interface graphique générée automatique : [Swagger](http://localhost:8080/swagger-ui.html).

L'application principale tentant de répondre au besoin est accessible [ici](http://localhost:8080/).

### Tests unitaires

Les tests unitaires peuvent être lancés avec la commande suivante :

```
 $ mvn test
```
