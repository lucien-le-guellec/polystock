package condtest.restapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;

import javax.persistence.Entity;
import javax.persistence.Table;

import java.util.Collection;

@Entity
@Table(name = "shelf")
public final class Shelf {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@ApiModelProperty(hidden = true)
	private Long id;

	@Column(name = "name")
	private String name;

	@ManyToOne
	@JoinColumn(name = "magasin_id", nullable=true)
//	@JoinColumn(name = "magasin_id", nullable=false)
	@JsonIgnore
	private Magasin magasin;

	@OneToMany (fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name = "shelf_id")
	@JsonIgnore
	private Collection<Article> articles;

	@PrePersist
	private void onCreate() {
	}

	@PreUpdate
	private void onUpdate() {
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Magasin getMagasin() {
		return magasin;
	}

	public void setMagasin(final Magasin magasin) {
		this.magasin = magasin;
	}

	public Collection<Article> getArticles() {
		return articles;
	}

	public void setArticles(final Collection<Article> articles) {
		this.articles = articles;
	}
}
