package condtest.restapi.service;

import java.util.Optional;

import condtest.restapi.entity.Shelf;
import condtest.restapi.exception.EntityWithNameAlreadyExistsException;
import condtest.restapi.exception.EntityNotFoundException;

public interface ShelfService {

	Iterable<Shelf> findAllByMagasin(final Long magasinId);
	Optional<Shelf> findById(final Long id);
	Optional<Shelf> findByMagasinAndName(final Long magasinId, final String name);
	Shelf save(final Long magasinId, final Shelf m) throws EntityNotFoundException, EntityWithNameAlreadyExistsException;
	Shelf update(final Long id, final Shelf m) throws EntityNotFoundException, EntityWithNameAlreadyExistsException;
	void delete(final Long id) throws EntityNotFoundException;

}
