package condtest.restapi.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.lang.Iterable;

import condtest.restapi.entity.Article;
import condtest.restapi.entity.Magasin;
import condtest.restapi.entity.Shelf;
import condtest.restapi.service.ShelfService;
import condtest.restapi.repository.ArticleRepository;
import condtest.restapi.repository.MagasinRepository;
import condtest.restapi.repository.ShelfRepository;
import condtest.restapi.exception.EntityWithNameAlreadyExistsException;
import condtest.restapi.exception.EntityNotFoundException;

import org.springframework.stereotype.Service;

@Service(value = "ShelfService")
public class ShelfServiceImpl implements ShelfService {

	private final ArticleRepository articleRepository;
	private final ShelfRepository repository;
	private final MagasinRepository magasinRepository;

	public ShelfServiceImpl(final ShelfRepository r, final MagasinRepository mR, final ArticleRepository aR) {
		this.repository = r;
		this.magasinRepository = mR;
		this.articleRepository = aR;
	}

	private boolean isNameTaken(Shelf s) {
		return isNameTaken(s.getMagasin().getId(), s.getName());
	}

	private boolean isNameTaken(Long magasinid, String name) {
		for (Shelf m : this.repository.findByName(name))
			if (m.getMagasin().getId() == magasinid)
				return true;
		return false;
	}

	private Shelf getById(final Long id) throws EntityNotFoundException {
		final Optional<Shelf> o = this.repository.findById(id);
		if (!o.isPresent())
			throw new EntityNotFoundException(id);
		return o.get();
	}

	private Magasin getMagasinById(final Long id) throws EntityNotFoundException {
		final Optional<Magasin> o = this.magasinRepository.findById(id);
		if (!o.isPresent())
			throw new EntityNotFoundException(id);
		return o.get();
	}

	public Collection<Shelf> findAllByMagasin(final Long id) {
		Optional<Magasin> o = this.magasinRepository.findById(id);
		if (!o.isPresent())
			return new ArrayList();
		return o.get().getShelves();
	}

	public Optional<Shelf> findById(final Long id) {
		return this.repository.findById(id);
	}

	public Optional<Shelf> findByMagasinAndName(final Long magasinId, final String name) {
		for (Shelf m : this.repository.findByName(name))
			if (m.getMagasin().getId() == magasinId)
				return Optional.of(m);
		return Optional.empty();
	}

	public Shelf save(final Long magasinId, final Shelf s) throws EntityNotFoundException, EntityWithNameAlreadyExistsException {
		s.setMagasin(getMagasinById(magasinId));
		if (this.isNameTaken(s))
			throw new EntityWithNameAlreadyExistsException(s.getName());
		return this.repository.save(s);
	}

	public Shelf update(final Long id, final Shelf s) throws EntityNotFoundException, EntityWithNameAlreadyExistsException {
		final Shelf old = this.getById(id);
		if (!old.getName().equals(s.getName()) && this.isNameTaken(s))
			throw new EntityWithNameAlreadyExistsException(s.getName());

		// modification de chaque champ de l'entité
		old.setName(s.getName());
		return this.repository.save(old);
	}

	public void delete(final Long id) throws EntityNotFoundException {
		final Shelf s = this.getById(id);
		for (Article a : s.getArticles())
			this.articleRepository.delete(a);
		this.repository.delete(s);
	}

}
