package condtest.restapi.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.lang.Iterable;

import condtest.restapi.entity.Shelf;
import condtest.restapi.entity.Article;
import condtest.restapi.service.ArticleService;
import condtest.restapi.repository.ArticleRepository;
import condtest.restapi.repository.ShelfRepository;
import condtest.restapi.exception.EntityNotFoundException;

import org.springframework.stereotype.Service;

@Service(value = "ArticleService")
public class ArticleServiceImpl implements ArticleService {

	private final ArticleRepository repository;
	private final ShelfRepository shelfRepository;

	public ArticleServiceImpl(final ArticleRepository aR, final ShelfRepository sR) {
		this.repository = aR;
		this.shelfRepository = sR;
	}

	private Article getById(final Long id) throws EntityNotFoundException {
		final Optional<Article> o = this.repository.findById(id);
		if (!o.isPresent())
			throw new EntityNotFoundException(id);
		return o.get();
	}

	private Shelf getShelfById(final Long id) throws EntityNotFoundException {
		final Optional<Shelf> o = this.shelfRepository.findById(id);
		if (!o.isPresent())
			throw new EntityNotFoundException(id);
		return o.get();
	}

	public Collection<Article> findAllByShelf(final Long id) {
		Optional<Shelf> o = this.shelfRepository.findById(id);
		if (!o.isPresent())
			return new ArrayList();
		return o.get().getArticles();
	}

	public Optional<Article> findById(final Long id) {
		return this.repository.findById(id);
	}

	public Article save(final Long shelfId, final Article s) throws EntityNotFoundException {
		s.setShelf(getShelfById(shelfId));
		return this.repository.save(s);
	}

	public Article update(final Long id, final Article s) throws EntityNotFoundException {
		final Article old = this.getById(id);

		// modification de chaque champ de l'entité
		old.setName(s.getName());
		old.setReference(s.getReference());
		old.setQuantity(s.getQuantity());
		return this.repository.save(old);
	}

	public void delete(final Long id) throws EntityNotFoundException {
		this.repository.delete(this.getById(id));
	}

}
