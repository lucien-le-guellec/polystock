package condtest.restapi.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.lang.Iterable;

import condtest.restapi.entity.Magasin;
import condtest.restapi.entity.Article;
import condtest.restapi.entity.Shelf;
import condtest.restapi.service.MagasinService;
import condtest.restapi.repository.MagasinRepository;
import condtest.restapi.repository.ShelfRepository;
import condtest.restapi.repository.ArticleRepository;
import condtest.restapi.exception.EntityWithNameAlreadyExistsException;
import condtest.restapi.exception.EntityNotFoundException;

import org.springframework.stereotype.Service;

@Service(value = "magasinService")
public class MagasinServiceImpl implements MagasinService {

	private final MagasinRepository repository;
	private final ShelfRepository shelfRepository;
	private final ArticleRepository articleRepository;

	public MagasinServiceImpl(final MagasinRepository r, final ShelfRepository s, final ArticleRepository a) {
		this.repository = r;
		this.shelfRepository = s;
		this.articleRepository = a;
	}

	private boolean isNameTaken(String name) {
		for (Magasin m : this.repository.findByName(name))
			return true;
		return false;
//		return !this.repository.findByName(name).isEmpty();
	}

	private Magasin getById(final Long id) throws EntityNotFoundException {
		final Optional<Magasin> o = this.repository.findById(id);
		if (!o.isPresent())
			throw new EntityNotFoundException(id);
		return o.get();
	}

	public Iterable<Magasin> findAll() {
		return this.repository.findAll();
	}

	public Optional<Magasin> findById(final Long id) {
		return this.repository.findById(id);
	}

	public Optional<Magasin> findByName(String name) {
		for (Magasin m : this.repository.findByName(name))
			return Optional.of(m);
		return Optional.empty();
	}

	public Magasin save(final Magasin m) throws EntityWithNameAlreadyExistsException {
		if (this.isNameTaken(m.getName()))
			throw new EntityWithNameAlreadyExistsException(m.getName());
		return this.repository.save(m);
	}

	public Magasin update(final Long id, final Magasin m) throws EntityNotFoundException, EntityWithNameAlreadyExistsException {
		final Magasin old = this.getById(id);
		if (!old.getName().equals(m.getName()) && this.isNameTaken(m.getName()))
			throw new EntityWithNameAlreadyExistsException(m.getName());

		// modification de chaque champ de l'entité
		old.setName(m.getName());
		old.setCity(m.getCity());
		return this.repository.save(old);
	}

	public void delete(final Long id) throws EntityNotFoundException {
		final Magasin m = this.getById(id);
		final Collection<Shelf> ss = m.getShelves();
		for (Shelf s : ss) {
			final Collection<Article> as = s.getArticles();
			for (Article a : as)
				this.articleRepository.delete(a);
			this.shelfRepository.delete(s);
		}
		this.repository.delete(m);
	}

}
