package condtest.restapi.service;

import java.util.Optional;

import condtest.restapi.entity.Magasin;
import condtest.restapi.exception.EntityWithNameAlreadyExistsException;
import condtest.restapi.exception.EntityNotFoundException;

public interface MagasinService {

	Iterable<Magasin> findAll();
	Optional<Magasin> findById(final Long id);
	Optional<Magasin> findByName(final String name);
	Magasin save(final Magasin m) throws EntityWithNameAlreadyExistsException;
	Magasin update(final Long id, final Magasin m) throws EntityNotFoundException, EntityWithNameAlreadyExistsException;
	void delete(final Long id) throws EntityNotFoundException;

}
