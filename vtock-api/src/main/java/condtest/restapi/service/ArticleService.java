package condtest.restapi.service;

import java.util.Optional;

import condtest.restapi.entity.Article;
import condtest.restapi.exception.EntityNotFoundException;

public interface ArticleService {

	Iterable<Article> findAllByShelf(final Long shelfId);
	Optional<Article> findById(final Long id);
	Article save(final Long shelfId, final Article m) throws EntityNotFoundException;
	Article update(final Long id, final Article m) throws EntityNotFoundException;
	void delete(final Long id) throws EntityNotFoundException;

}
