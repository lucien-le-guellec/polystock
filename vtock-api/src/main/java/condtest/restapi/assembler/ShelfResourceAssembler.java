package condtest.restapi.assembler;

import condtest.restapi.entity.Shelf;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Component
public class ShelfResourceAssembler implements ResourceAssembler<Shelf, Resource<Shelf>> {

	public Resources<Resource<Shelf>> toResources(final Iterable<Shelf> ms) {
		final ArrayList<Resource<Shelf>> al = new ArrayList<>();
		for (Shelf m : ms)
			al.add(this.toResource(m));
		return new Resources<>(al);
	}

	public Resources<Resource<Shelf>> toResources(final Collection<Shelf> ms) {
		final Collection<Resource<Shelf>> rs = ms.stream()
			.map(this::toResource)
			.collect(Collectors.toList());
		return new Resources<>(rs);
	}

	public Resource<Shelf> toResource(final Shelf m) {
		return new Resource<>(m);
	}
}
