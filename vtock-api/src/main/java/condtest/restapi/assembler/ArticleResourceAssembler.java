package condtest.restapi.assembler;

import condtest.restapi.entity.Article;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Component
public class ArticleResourceAssembler implements ResourceAssembler<Article, Resource<Article>> {

	public Resources<Resource<Article>> toResources(final Iterable<Article> ms) {
		final ArrayList<Resource<Article>> al = new ArrayList<>();
		for (Article m : ms)
			al.add(this.toResource(m));
		return new Resources<>(al);
	}

	public Resources<Resource<Article>> toResources(final Collection<Article> ms) {
		final Collection<Resource<Article>> rs = ms.stream()
			.map(this::toResource)
			.collect(Collectors.toList());
		return new Resources<>(rs);
	}

	public Resource<Article> toResource(final Article m) {
		return new Resource<>(m);
	}
}
