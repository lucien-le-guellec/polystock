package condtest.restapi.assembler;

import condtest.restapi.entity.Magasin;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Component
public class MagasinResourceAssembler implements ResourceAssembler<Magasin, Resource<Magasin>> {

	public Resources<Resource<Magasin>> toResources(final Iterable<Magasin> ms) {
		final ArrayList<Resource<Magasin>> al = new ArrayList<>();
		for (Magasin m : ms)
			al.add(this.toResource(m));
		return new Resources<>(al);
	}

	public Resources<Resource<Magasin>> toResources(final Collection<Magasin> ms) {
		final Collection<Resource<Magasin>> rs = ms.stream()
			.map(this::toResource)
			.collect(Collectors.toList());
		return new Resources<>(rs);
	}

	public Resource<Magasin> toResource(final Magasin m) {
		return new Resource<>(m);
	}
}
