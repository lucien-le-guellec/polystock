package condtest.restapi.controller.impl;

import condtest.restapi.assembler.MagasinResourceAssembler;
import condtest.restapi.entity.Magasin;
import condtest.restapi.service.MagasinService;
import condtest.restapi.exception.EntityNotFoundException;
import condtest.restapi.exception.EntityWithNameAlreadyExistsException;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;
import java.net.URI;
import java.net.URISyntaxException;

@Controller
public class LandingPageControllerImpl {

	@RequestMapping(path = "/")
	public ResponseEntity<String> showHomePage() {
		return new ResponseEntity<String>(FileIndexHtml.content, HttpStatus.OK);
	}

	@RequestMapping(path = "/js/main.js")
	public ResponseEntity<String> main() {
		return new ResponseEntity<String>(FileMainJs.content, HttpStatus.OK);
	}

	@RequestMapping(path = "/design.css")
	public ResponseEntity<String> design() {
		return new ResponseEntity<String>(FileDesignCss.content, HttpStatus.OK);
	}
}

