package condtest.restapi.controller.impl;

public class FileIndexHtml {

	static public final String content = "<!DOCTYPE html>\n"+
"<html>\n"+
"<head>\n"+
"	<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />\n"+
"	<title>CTRA</title>\n"+
"	<link rel=\"stylesheet\" type=\"text/css\" href=\"design.css\" title=\"default\" media=\"screen\" />\n"+
"</head>\n"+
"<body>\n"+
"	<nav><button id=\"bListMagasins\">Liste des magasins</button></nav>\n"+
"	<section id=\"sCenter\">\n"+
"		<div id=\"dummy\"></div>\n"+
"		<section id=\"listMagasins\">\n"+
"			<div class=\"divHeader\">\n"+
"				<div class=\"buttonoptions\">\n"+
"					<button id=\"bAddMagasin\">Ajouter un magasin</button>\n"+
"				</div>\n"+
"				<b>Liste des magasins</b>\n"+
"			</div>\n"+
"			<table>\n"+
"				<thead>\n"+
"					<tr><th>Nom</th></tr>\n"+
"				</thead>\n"+
"				<tbody id=\"tListMagasins\">\n"+
"					<tr><td>Goering</td></tr>\n"+
"				</tbody>\n"+
"			</table>\n"+
"		</section>\n"+
"		<section id=\"addMagasin\">\n"+
"			<h4>Magasin</h4>\n"+
"			<table class=\"tableform\">\n"+
"				<tr>\n"+
"					<td><label for=\"faddMagasinCity\">Ville</label> :</td>\n"+
"					<td><input type=\"text\" id=\"faddMagasinCity\" /></td>\n"+
"				</tr>\n"+
"				<tr>\n"+
"					<td><label for=\"faddMagasinName\">Nom</label> :</td>\n"+
"					<td><input type=\"text\" id=\"faddMagasinName\" /></td>\n"+
"				</tr>\n"+
"			</table>\n"+
"			<button id=\"bokaddMagasin\">Valider</button><button id=\"bcanceladdMagasin\">Annuler</button>\n"+
"		</section>\n"+
"		<section id=\"showMagasin\">\n"+
"			<div class=\"divHeader\">\n"+
"				<div id=\"showMagasinButtons\" class=\"buttonoptions\">\n"+
"					<button id=\"bAddShelf\">Ajouter un rayon</button><br />\n"+
"					<button id=\"bsetMagasin\">Modifier</button><br />\n"+
"					<button id=\"bdeleteMagasin\">Supprimer</button>\n"+
"				</div>\n"+
"				<b>Magasin de : <span id=\"showMagasinDisplayCity\"></span></b><br />\n"+
"				<b>Nom : <span id=\"showMagasinDisplayName\"></span></b>\n"+
"			</div>\n"+
"			<table>\n"+
"				<thead>\n"+
"					<tr><th>Nom de rayon</th></tr>\n"+
"				</thead>\n"+
"				<tbody id=\"tListShelves\"></tbody>\n"+
"			</table>\n"+
"		</section>\n"+
"		<section id=\"setMagasin\">\n"+
"			<h4>Magasin</h4>\n"+
"			<table class=\"tableform\">\n"+
"				<tr>\n"+
"					<td><label for=\"fsetMagasinCity\">Ville</label> :</td>\n"+
"					<td><input type=\"text\" id=\"fsetMagasinCity\" /></td>\n"+
"				</tr>\n"+
"				<tr>\n"+
"					<td><label for=\"fsetMagasinName\">Nom</label> :</td>\n"+
"					<td><input type=\"text\" id=\"fsetMagasinName\" /></td>\n"+
"				</tr>\n"+
"			</table>\n"+
"			<div id=\"buttonssetMagasin\"><button id=\"boksetMagasin\">Valider</button><button id=\"bcancelsetMagasin\">Annuler</button></div>\n"+
"		</section>\n"+
"		<section id=\"addShelf\">\n"+
"			<h4>Rayon du magasin <span id=\"addShelfDisplayMagasinName\"></span></h4>\n"+
"			<table class=\"tableform\">\n"+
"				<tr>\n"+
"					<td><label for=\"faddShelfName\">Nom</label> :</td>\n"+
"					<td><input type=\"text\" id=\"faddShelfName\" /></td>\n"+
"				</tr>\n"+
"			</table>\n"+
"			<button id=\"bokaddShelf\">Valider</button><button id=\"bcanceladdShelf\">Annuler</button>\n"+
"		</section>\n"+
"		<section id=\"showShelf\">\n"+
"			<div class=\"divHeader\">\n"+
"				<div id=\"showShelfButtons\" class=\"buttonoptions\">\n"+
"					<button id=\"bAddArticle\">Ajouter un article</button><br />\n"+
"					<button id=\"bsetShelf\">Modifier</button><br />\n"+
"					<button id=\"bdeleteShelf\">Supprimer</button>\n"+
"				</div>\n"+
"				<b>Rayon du magasin : <span id=\"showShelfDisplayMagasinName\"></span></b><br />\n"+
"				<b>Nom : <span id=\"showShelfDisplayName\"></span></b>\n"+
"			</div>\n"+
"			<table>\n"+
"				<thead>\n"+
"					<tr><th>Référence</th><th>Nom</th><th>Quantité</th></tr>\n"+
"				</thead>\n"+
"				<tbody id=\"tListArticles\"></tbody>\n"+
"			</table>\n"+
"		</section>\n"+
"		<section id=\"setShelf\">\n"+
"			<h4>Rayon du magasin : <span id=\"setShelfDisplayMagasinName\"></span></h4>\n"+
"			<table class=\"tableform\">\n"+
"				<tr>\n"+
"					<td><label for=\"fsetShelfName\">Nom</label> :</td>\n"+
"					<td><input type=\"text\" id=\"fsetShelfName\" /></td>\n"+
"				</tr>\n"+
"			</table>\n"+
"			<div id=\"buttonssetShelf\"><button id=\"boksetShelf\">Valider</button><button id=\"bcancelsetShelf\">Annuler</button></div>\n"+
"		</section>\n"+
"		<section id=\"addArticle\">\n"+
"			<h4>Article du rayon <span id=\"addArticleDisplayShelfName\"></span> du magasin <span id=\"addArticleDisplayMagasinName\"></span></h4>\n"+
"			<table class=\"tableform\">\n"+
"				<tr>\n"+
"					<td><label for=\"faddArticleName\">Nom</label> :</td>\n"+
"					<td><input type=\"text\" id=\"faddArticleName\" /></td>\n"+
"				</tr>\n"+
"				<tr>\n"+
"					<td><label for=\"faddArticleReference\">Référence</label> :</td>\n"+
"					<td><input type=\"text\" id=\"faddArticleReference\" /></td>\n"+
"				</tr>\n"+
"				<tr>\n"+
"					<td><label for=\"faddArticleQuantity\">Quantité</label> :</td>\n"+
"					<td><input type=\"text\" id=\"faddArticleQuantity\" /></td>\n"+
"				</tr>\n"+
"			</table>\n"+
"			<button id=\"bokaddArticle\">Valider</button><button id=\"bcanceladdArticle\">Annuler</button>\n"+
"		</section>\n"+
"		<section id=\"showArticle\">\n"+
"			<div class=\"divHeader\">\n"+
"				<div id=\"showArticleButtons\" class=\"buttonoptions\">\n"+
"					<button id=\"bsetArticle\">Modifier</button><br />\n"+
"					<button id=\"bdeleteArticle\">Supprimer</button>\n"+
"				</div>\n"+
"				<b>Article du Rayon <span id=\"showArticleDisplayShelfName\"></span> du magasin : <span id=\"showArticleDisplayMagasinName\"></span></b><br />\n"+
"				<b>Nom : <span id=\"showArticleDisplayName\"></span></b><br />\n"+
"				<b>Référence : <span id=\"showArticleDisplayReference\"></span></b><br />\n"+
"				<b>Quantité : <span id=\"showArticleDisplayQuantity\"></span></b>\n"+
"			</div>\n"+
"		</section>\n"+
"		<section id=\"setArticle\">\n"+
"			<h4>Article du rayon <span id=\"setArticleDisplayShelfName\"></span> du magasin <span id=\"setArticleDisplayMagasinName\"></span></h4>\n"+
"			<table class=\"tableform\">\n"+
"				<tr>\n"+
"					<td><label for=\"fsetArticleName\">Nom</label> :</td>\n"+
"					<td><input type=\"text\" id=\"fsetArticleName\" /></td>\n"+
"				</tr>\n"+
"				<tr>\n"+
"					<td><label for=\"fsetArticleReference\">Référence</label> :</td>\n"+
"					<td><input type=\"text\" id=\"fsetArticleReference\" /></td>\n"+
"				</tr>\n"+
"				<tr>\n"+
"					<td><label for=\"fsetArticleQuantity\">Quantité</label> :</td>\n"+
"					<td><input type=\"text\" id=\"fsetArticleQuantity\" /></td>\n"+
"				</tr>\n"+
"			</table>\n"+
"			<button id=\"boksetArticle\">Valider</button><button id=\"bcancelsetArticle\">Annuler</button>\n"+
"		</section>\n"+
"	</section>\n"+
"	<script src=\"js/main.js\"></script>\n"+
"</body>\n"+
"</html>";

}
