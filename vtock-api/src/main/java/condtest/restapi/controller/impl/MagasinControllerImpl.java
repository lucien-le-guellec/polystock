package condtest.restapi.controller.impl;

import condtest.restapi.assembler.MagasinResourceAssembler;
import condtest.restapi.assembler.ShelfResourceAssembler;
import condtest.restapi.entity.Magasin;
import condtest.restapi.entity.Shelf;
import condtest.restapi.service.MagasinService;
import condtest.restapi.service.ShelfService;
import condtest.restapi.exception.EntityNotFoundException;
import condtest.restapi.exception.EntityWithNameAlreadyExistsException;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;
import java.util.ArrayList;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "/magasin/")
public class MagasinControllerImpl {

	static final private String FORMAT_ID_NOT_FOUND = "{\"error\":\"Entity with id [%d] not found\"}";
	static final private String FORMAT_NAME_NOT_FOUND = "{\"error\":\"Entity with name [%s] not found\"}";
	static final private String FORMAT_NAME_CONFLICT = "{\"error\":\"Entity with name [%s] already exists\"}";
	static final private String FORMAT_MAGASIN = "{\"id\":%d,\"name\":\"%s\",\"city\":\"%s\"}";
	static final private String FORMAT_SHELF = "{\"id\":%d,\"name\":\"%s\"}";

	private final MagasinService magasinService;
	private final ShelfService shelfService;

	private final MagasinResourceAssembler magasinAssembler;
	private final ShelfResourceAssembler shelfAssembler;

	public MagasinControllerImpl(final MagasinService mS, final MagasinResourceAssembler rA, final ShelfService sS, final ShelfResourceAssembler sA) {
		this.magasinAssembler = rA;
		this.magasinService = mS;
		this.shelfAssembler = sA;
		this.shelfService = sS;
	}

	private ResponseEntity<String> idNotFound(Long id) {
		return new ResponseEntity<String>(String.format(FORMAT_ID_NOT_FOUND, id), HttpStatus.NOT_FOUND);
	}

	private ResponseEntity<String> nameConflict(String name) {
		return new ResponseEntity<String>(String.format(FORMAT_NAME_CONFLICT, name), HttpStatus.CONFLICT);
	}

	private ResponseEntity<String> returnMagasin(Magasin m) {
		return returnMagasin(m, HttpStatus.OK);
	}

	private ResponseEntity<String> returnMagasin(Magasin m, HttpStatus status) {
		return new ResponseEntity<String>(String.format(FORMAT_MAGASIN, m.getId(), m.getName(), m.getCity()), status);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Resources<Resource<Magasin>> all() {
		return this.magasinAssembler.toResources(this.magasinService.findAll());
	}

	@GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> one(@PathVariable final Long id) {
		Optional<Magasin> o = this.magasinService.findById(id);
		if (!o.isPresent())
			return this.idNotFound(id);
		return returnMagasin(o.get(), HttpStatus.OK);
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@RequestBody final Magasin entity) throws URISyntaxException {
		Magasin m;
		try {
			m = this.magasinService.save(entity);
		} catch(EntityWithNameAlreadyExistsException e) {
			return this.nameConflict(entity.getName());
		}
		return returnMagasin(m, HttpStatus.CREATED);
	}

	@PutMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@PathVariable final Long id, @RequestBody final Magasin entity) throws URISyntaxException {
		Magasin m;
		try {
			m = this.magasinService.update(id, entity);
		} catch(EntityNotFoundException e) {
			return this.idNotFound(id);
		} catch(EntityWithNameAlreadyExistsException e) {
			return this.nameConflict(entity.getName());
		}
		return returnMagasin(m, HttpStatus.ACCEPTED);
	}

	@DeleteMapping(path = "{id}")
	public ResponseEntity<String> delete(@PathVariable final Long id) {
		try {
			this.magasinService.delete(id);
		} catch(EntityNotFoundException e) {
			return idNotFound(id);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	private ResponseEntity<String> returnShelf(Shelf m) {
		return returnShelf(m, HttpStatus.OK);
	}

	private ResponseEntity<String> returnShelf(Shelf m, HttpStatus status) {
		return new ResponseEntity<String>(String.format(FORMAT_SHELF, m.getId(), m.getName()), status);
	}

	@GetMapping(path = "{id}/shelves", produces = MediaType.APPLICATION_JSON_VALUE)
	public Resources<Resource<Shelf>> allShelves(@PathVariable final Long id) {
		try {
			return this.shelfAssembler.toResources(this.shelfService.findAllByMagasin(id));
		} catch(EntityNotFoundException e) {
			return this.shelfAssembler.toResources(new ArrayList());
		}
	}

	@PostMapping(path = "{id}/shelves", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@PathVariable final Long id, @RequestBody final Shelf entity) throws URISyntaxException {
		Shelf s;
		try {
			s = this.shelfService.save(id, entity);
		} catch(EntityWithNameAlreadyExistsException e) {
			return this.nameConflict(entity.getName());
		}
		return returnShelf(s, HttpStatus.CREATED);
	}
}

