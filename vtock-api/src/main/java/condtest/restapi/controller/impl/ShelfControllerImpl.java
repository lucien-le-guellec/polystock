package condtest.restapi.controller.impl;

import condtest.restapi.assembler.ShelfResourceAssembler;
import condtest.restapi.entity.Shelf;
import condtest.restapi.service.ShelfService;
import condtest.restapi.assembler.ArticleResourceAssembler;
import condtest.restapi.entity.Article;
import condtest.restapi.service.ArticleService;
import condtest.restapi.exception.EntityNotFoundException;
import condtest.restapi.exception.EntityWithNameAlreadyExistsException;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "/shelf/")
public class ShelfControllerImpl {

	static final private String FORMAT_ID_NOT_FOUND = "{\"error\":\"Entity with id [%d] not found\"}";
	static final private String FORMAT_NAME_NOT_FOUND = "{\"error\":\"Entity with name [%s] not found\"}";
	static final private String FORMAT_NAME_CONFLICT = "{\"error\":\"Entity with name [%s] already exists\"}";
	static final private String FORMAT_SHELF = "{\"id\":%d,\"name\":\"%s\"}";
	static final private String FORMAT_ARTICLE = "{\"id\":%d,\"name\":\"%s\",\"reference\":\"%s\",\"quantity\":%d}";

	private final ShelfService shelfService;
	private final ArticleService articleService;

	private final ShelfResourceAssembler shelfAssembler;
	private final ArticleResourceAssembler articleAssembler;

	public ShelfControllerImpl(final ShelfService sS, final ShelfResourceAssembler sA, final ArticleService aS, final ArticleResourceAssembler aA) {
		this.shelfAssembler = sA;
		this.shelfService = sS;
		this.articleAssembler = aA;
		this.articleService = aS;
	}

	private ResponseEntity<String> idNotFound(Long id) {
		return new ResponseEntity<String>(String.format(FORMAT_ID_NOT_FOUND, id), HttpStatus.NOT_FOUND);
	}

	private ResponseEntity<String> nameConflict(String name) {
		return new ResponseEntity<String>(String.format(FORMAT_NAME_CONFLICT, name), HttpStatus.CONFLICT);
	}

	private ResponseEntity<String> returnShelf(Shelf m) {
		return returnShelf(m, HttpStatus.OK);
	}

	private ResponseEntity<String> returnShelf(Shelf m, HttpStatus status) {
		return new ResponseEntity<String>(String.format(FORMAT_SHELF, m.getId(), m.getName()), status);
	}

	private ResponseEntity<String> returnArticle(Article m) {
		return returnArticle(m, HttpStatus.OK);
	}

	private ResponseEntity<String> returnArticle(Article m, HttpStatus status) {
		return new ResponseEntity<String>(String.format(FORMAT_ARTICLE, m.getId(), m.getName(), m.getReference(), m.getQuantity()), status);
	}

	@GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> one(@PathVariable final Long id) {
		Optional<Shelf> o = this.shelfService.findById(id);
		if (!o.isPresent())
			return this.idNotFound(id);
		return returnShelf(o.get(), HttpStatus.OK);
	}

	@PutMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@PathVariable final Long id, @RequestBody final Shelf entity) throws URISyntaxException {
		Shelf m;
		try {
			m = this.shelfService.update(id, entity);
		} catch(EntityNotFoundException e) {
			return this.idNotFound(id);
		} catch(EntityWithNameAlreadyExistsException e) {
			return this.nameConflict(entity.getName());
		}
		return returnShelf(m, HttpStatus.ACCEPTED);
	}

	@DeleteMapping(path = "{id}")
	public ResponseEntity<String> delete(@PathVariable final Long id) {
		try {
			this.shelfService.delete(id);
		} catch(EntityNotFoundException e) {
			return idNotFound(id);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping(path = "{id}/articles", produces = MediaType.APPLICATION_JSON_VALUE)
	public Resources<Resource<Article>> allShelves(@PathVariable final Long id) {
		return this.articleAssembler.toResources(this.articleService.findAllByShelf(id));
	}

	@PostMapping(path = "{id}/articles", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> create(@PathVariable final Long id, @RequestBody final Article entity) throws URISyntaxException {
		Article a;
		try {
			a = this.articleService.save(id, entity);
		} catch(EntityNotFoundException e) {
			return this.idNotFound(id);
		}
		return returnArticle(a, HttpStatus.CREATED);
	}

}
