package condtest.restapi.controller.impl;

public class FileDesignCss {

	static public final String content = "body { "+
"	background-color: #afafdc; "+
"} "+
"nav { "+
"	margin: 10px; "+
"	padding: 10px; "+
"	border: 1px black solid; "+
"} "+
"section { "+
"	margin: 10px; "+
"	padding: 10px; "+
"	display: none; "+
"} "+
"table { "+
"	border-collapse: collapse; "+
"	width: 100%; "+
"	margin-top: 20px; "+
"	margin-bottom: 20px; "+
"} "+
"button { "+
"	margin-left: 20px; "+
"	margin-bottom: 5px; "+
"} "+
"td, th { "+
"	border: 1px black solid; "+
"	padding-left: 5px; "+
"} "+
".buttonoptions { "+
"	float: right;	 "+
"} "+
".tableform td, .tableform th { "+
"	border: none; "+
"} "+
".divHeader { "+
"	padding: 15px; "+
"	margin-bottom: 10px; "+
"	border: 1px black solid; "+
"} "+
"#sCenter { "+
"	padding-top: 0px; "+
"	padding-left: 0px; "+
"	padding-right: 0px; "+
"	border: 1px black solid; "+
"	display: block; "+
"} "+
"#dummy { "+
"	border-bottom: 1px black solid; "+
"	height: 20px; "+
"} "+
"#listMagasins { "+
"	margin: 20px; "+
"	padding-right: 0px; "+
"	padding-left: 0px; "+
"} ";

}
