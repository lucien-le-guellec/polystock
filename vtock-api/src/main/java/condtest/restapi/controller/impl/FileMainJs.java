package condtest.restapi.controller.impl;

public class FileMainJs {

	static public final String content = "function addEvent(element, event, func) {\n"+
"	if (element.addEventListener) {\n"+
"		element.addEventListener(event, func, false);\n"+
"	} else {\n"+
"		element.attachEvent('on' + event, func);\n"+
"	}\n"+
"}\n"+
"var knowledge = {\n"+
"	magasin: {id: 0, name: \"\", city: \"\"},\n"+
"	shelf: {id: 0, name: \"\"},\n"+
"	article: {id: 0, name: \"\", reference: \"\", quantity: 0}\n"+
"};\n"+
"function updateListOfMagasins(magasins) {\n"+
"	// tListMagasins\n"+
"	var tl = document.getElementById(\"tListMagasins\");\n"+
"	tl.innerHTML = \"\";\n"+
"	for (var i = 0, l = magasins.length; i < l; i++) {\n"+
"		var span = document.createElement(\"span\");\n"+
"		span.appendChild(document.createTextNode(magasins[i].name));\n"+
"		span.id = magasins[i].id;\n"+
"		addEvent(span, \"click\", (function() { var id = magasins[i].id; return function() { gotoShowMagasin(id); }; })());\n"+
"		var td = document.createElement(\"td\");\n"+
"		td.appendChild(span);\n"+
"		var tr = document.createElement(\"tr\");\n"+
"		tr.appendChild(td);\n"+
"		tl.appendChild(tr);\n"+
"	}\n"+
"}\n"+
"function updateMagasin(magasin) {\n"+
"	var city = magasin.city;\n"+
"	var name = magasin.name;\n"+
"	document.getElementById(\"showMagasinDisplayCity\").innerHTML = city;\n"+
"	document.getElementById(\"showMagasinDisplayName\").innerHTML = name;\n"+
"	document.getElementById(\"addShelfDisplayMagasinName\").innerHTML = name;\n"+
"	document.getElementById(\"showShelfDisplayMagasinName\").innerHTML = name;\n"+
"	document.getElementById(\"setShelfDisplayMagasinName\").innerHTML = name;\n"+
"	document.getElementById(\"addArticleDisplayMagasinName\").innerHTML = name;\n"+
"	document.getElementById(\"showArticleDisplayMagasinName\").innerHTML = name;\n"+
"	document.getElementById(\"setArticleDisplayMagasinName\").innerHTML = name;\n"+
"}\n"+
"function updateListOfShelves(shelves) {\n"+
"	// tListShelves\n"+
"	var tl = document.getElementById(\"tListShelves\");\n"+
"	tl.innerHTML = \"\";\n"+
"	for (var i = 0, l = shelves.length; i < l; i++) {\n"+
"		var span = document.createElement(\"span\");\n"+
"		span.appendChild(document.createTextNode(shelves[i].name));\n"+
"		span.id = shelves[i].id;\n"+
"		addEvent(span, \"click\", (function() { var id = shelves[i].id; return function() { gotoShowShelf(id); }; })());\n"+
"		var td = document.createElement(\"td\");\n"+
"		td.appendChild(span);\n"+
"		var tr = document.createElement(\"tr\");\n"+
"		tr.appendChild(td);\n"+
"		tl.appendChild(tr);\n"+
"	}\n"+
"}\n"+
"function updateShelf(shelf) {\n"+
"	var name = shelf.name;\n"+
"	document.getElementById(\"showArticleDisplayShelfName\").innerHTML = name;\n"+
"	document.getElementById(\"setArticleDisplayShelfName\").innerHTML = name;\n"+
"	document.getElementById(\"addArticleDisplayShelfName\").innerHTML = name;\n"+
"	document.getElementById(\"showShelfDisplayName\").innerHTML = name;\n"+
"}\n"+
"function updateListOfArticles(articles) {\n"+
"	// tListArticles\n"+
"	var tl = document.getElementById(\"tListArticles\");\n"+
"	tl.innerHTML = \"\";\n"+
"	for (var i = 0, l = articles.length; i < l; i++) {\n"+
"		var span = document.createElement(\"span\");\n"+
"		span.appendChild(document.createTextNode(articles[i].reference));\n"+
"		span.id = articles[i].id;\n"+
"		addEvent(span, \"click\", (function() { var id = articles[i].id; return function() { gotoShowArticle(id); }; })());\n"+
"		var td = document.createElement(\"td\");\n"+
"		td.appendChild(span);\n"+
"		var tr = document.createElement(\"tr\");\n"+
"		tr.appendChild(td);\n"+
"		span = document.createElement(\"span\");\n"+
"		span.id = articles[i].id;\n"+
"		span.appendChild(document.createTextNode(articles[i].name));\n"+
"		addEvent(span, \"click\", (function() { var id = articles[i].id; return function() { gotoShowArticle(id); }; })());\n"+
"		td = document.createElement(\"td\");\n"+
"		td.appendChild(span);\n"+
"		tr.appendChild(td);\n"+
"		td = document.createElement(\"td\");\n"+
"		td.appendChild(document.createTextNode(articles[i].quantity));\n"+
"		tr.appendChild(td);\n"+
"		tl.appendChild(tr);\n"+
"	}\n"+
"}\n"+
"function updateArticle(article) {\n"+
"	document.getElementById(\"showArticleDisplayName\").innerHTML = article.name;\n"+
"	document.getElementById(\"showArticleDisplayReference\").innerHTML = article.reference;\n"+
"	document.getElementById(\"showArticleDisplayQuantity\").innerHTML = article.quantity;\n"+
"}\n"+
"function gotoShowMagasin(idMagasin) {\n"+
"	queryMagasin(idMagasin);\n"+
"	queryListOfShelves(idMagasin);\n"+
"	document.getElementById(\"showMagasin\").style.display = \"block\";\n"+
"	document.getElementById(\"listMagasins\").style.display = \"none\";\n"+
"}\n"+
"function gotoShowShelf(idShelf) {\n"+
"	queryShelf(idShelf);\n"+
"	queryListOfArticles(idShelf);\n"+
"	document.getElementById(\"showShelf\").style.display = \"block\";\n"+
"	document.getElementById(\"showMagasin\").style.display = \"none\";\n"+
"}\n"+
"function gotoShowArticle(idArticle) {\n"+
"	queryArticle(idArticle);\n"+
"	document.getElementById(\"showArticle\").style.display = \"block\";\n"+
"	document.getElementById(\"showShelf\").style.display = \"none\";\n"+
"}\n"+
"function queryListOfMagasins() {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 200) {\n"+
"			var liste = JSON.parse(this.responseText)._embedded;\n"+
"			if (liste == undefined) { updateListOfMagasins([]); return; }\n"+
"			liste = liste.magasinList;\n"+
"			updateListOfMagasins(liste);\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"GET\", \"magasin/\", true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function queryMagasin(idMagasin) {\n"+
"	if (idMagasin != null)\n"+
"		knowledge.magasin.id = idMagasin;\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 200) {\n"+
"			var magasin = JSON.parse(this.responseText);\n"+
"			knowledge.magasin = magasin;\n"+
"			updateMagasin(magasin);\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"GET\", \"magasin/\"+knowledge.magasin.id, true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function queryDeleteMagasin() {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 204) {\n"+
"			console.log(\"suppression d'un magasin avec succès\");\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"DELETE\", \"magasin/\"+knowledge.magasin.id, true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function querySetMagasin(magasin) {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4)\n"+
"			if (this.status == 202) {\n"+
"				var magasin_ = JSON.parse(this.responseText);\n"+
"				updateMagasin(magasin);\n"+
"				knowledge.magasin = magasin;\n"+
"				knowledge.shelf = {id: 0, name: \"\"};\n"+
"				knowledge.article = {id: 0, name: \"\", reference: \"\", quantity: 0};\n"+
"				document.getElementById(\"showMagasin\").style.display = \"block\";\n"+
"				document.getElementById(\"setMagasin\").style.display = \"none\";\n"+
"			} else if (this.status == 409) {\n"+
"				alert(JSON.parse(this.responseText).error);\n"+
"			}\n"+
"	};\n"+
"	xhttp.open(\"PUT\", \"magasin/\"+magasin.id, true);\n"+
"	xhttp.setRequestHeader(\"Content-type\", \"application/json\");\n"+
"	xhttp.send(\"{\\\"name\\\":\\\"\"+magasin.name+\"\\\", \\\"city\\\":\\\"\"+magasin.city+\"\\\"}\");\n"+
"}\n"+
"function queryAddMagasin(magasin) {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4)\n"+
"			if (this.status == 201) {\n"+
"				var magasin_ = JSON.parse(this.responseText);\n"+
"				knowledge.magasin = magasin;\n"+
"				knowledge.magasin.id = magasin_.id;\n"+
"				knowledge.shelf = {id: 0, name: \"\"};\n"+
"				knowledge.article = {id: 0, name: \"\", reference: \"\", quantity: 0};\n"+
"				document.getElementById(\"showMagasin\").style.display = \"block\";\n"+
"				document.getElementById(\"addMagasin\").style.display = \"none\";\n"+
"				updateMagasin(knowledge.magasin);\n"+
"			} else if (this.status == 409) {\n"+
"				alert(JSON.parse(this.responseText).error);\n"+
"			}\n"+
"	};\n"+
"	xhttp.open(\"POST\", \"magasin/\", true);\n"+
"	xhttp.setRequestHeader(\"Content-type\", \"application/json\");\n"+
"	xhttp.send(\"{\\\"name\\\":\\\"\"+magasin.name+\"\\\", \\\"city\\\":\\\"\"+magasin.city+\"\\\"}\"); \n"+
"}\n"+
"function queryListOfShelves(idMagasin) {\n"+
"	if (idMagasin != null)\n"+
"		knowledge.magasin.id = idMagasin;\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 200) {\n"+
"			var liste = JSON.parse(this.responseText)._embedded;\n"+
"			if (liste == undefined) { updateListOfShelves([]); return; }\n"+
"			liste = liste.shelfList;\n"+
"			updateListOfShelves(liste);\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"GET\", \"magasin/\"+(knowledge.magasin.id)+\"/shelves\", true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function queryShelf(idShelf) {\n"+
"	if (idShelf != null)\n"+
"		knowledge.shelf.id = idShelf;\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 200) {\n"+
"			var shelf = JSON.parse(this.responseText);\n"+
"			knowledge.shelf = shelf;\n"+
"			updateShelf(shelf);\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"GET\", \"shelf/\"+knowledge.shelf.id, true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function queryDeleteShelf() {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 204) {\n"+
"			console.log(\"suppression d'un rayon avec succès\");\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"DELETE\", \"shelf/\"+knowledge.shelf.id, true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function querySetShelf(shelf) {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4)\n"+
"			if (this.status == 202) {\n"+
"				var shelf = JSON.parse(this.responseText);\n"+
"				updateShelf(shelf);\n"+
"				knowledge.shelf = shelf;\n"+
"				knowledge.article = {id: 0, name: \"\", reference: \"\", quantity: 0};\n"+
"				document.getElementById(\"showShelf\").style.display = \"block\";\n"+
"				document.getElementById(\"setShelf\").style.display = \"none\";\n"+
"			} else if (this.status == 409) {\n"+
"				alert(JSON.parse(this.responseText).error);\n"+
"			}\n"+
"	};\n"+
"	xhttp.open(\"PUT\", \"shelf/\"+shelf.id, true);\n"+
"	xhttp.setRequestHeader(\"Content-type\", \"application/json\");\n"+
"	xhttp.send(\"{\\\"name\\\":\\\"\"+shelf.name+\"\\\"}\");\n"+
"}\n"+
"function queryAddShelf(shelf) {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4)\n"+
"			if (this.status == 201) {\n"+
"				var shelf = JSON.parse(this.responseText);\n"+
"				updateShelf(shelf);\n"+
"				knowledge.shelf = shelf;\n"+
"				knowledge.article = {id: 0, name: \"\", reference: \"\", quantity: 0};\n"+
"				document.getElementById(\"showShelf\").style.display = \"block\";\n"+
"				document.getElementById(\"addShelf\").style.display = \"none\";\n"+
"			} else if (this.status == 409) {\n"+
"				alert(JSON.parse(this.responseText).error);\n"+
"			}\n"+
"	};\n"+
"	xhttp.open(\"POST\", \"magasin/\"+knowledge.magasin.id+\"/shelves\", true);\n"+
"	xhttp.setRequestHeader(\"Content-type\", \"application/json\");\n"+
"	xhttp.send(\"{\\\"name\\\":\\\"\"+shelf.name+\"\\\"}\");\n"+
"}\n"+
"function queryListOfArticles(idShelf) {\n"+
"	if (idShelf != null)\n"+
"		knowledge.shelf.id = idShelf;\n"+
"	\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 200) {\n"+
"			var liste = JSON.parse(this.responseText)._embedded;\n"+
"			if (liste == undefined) { updateListOfArticles([]); return; }\n"+
"			liste = liste.articleList;\n"+
"			updateListOfArticles(liste);\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"GET\", \"shelf/\"+(knowledge.shelf.id)+\"/articles\", true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function queryArticle(idArticle) {\n"+
"	if (idArticle != null)\n"+
"		knowledge.article.id = idArticle;\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 200) {\n"+
"			var article = JSON.parse(this.responseText);\n"+
"			knowledge.article = article;\n"+
"			updateArticle(article);\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"GET\", \"article/\"+knowledge.article.id, true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function queryDeleteArticle() {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4 && this.status == 204) {\n"+
"			console.log(\"suppression d'un article avec succès\");\n"+
"		}\n"+
"	};\n"+
"	xhttp.open(\"DELETE\", \"article/\"+knowledge.article.id, true);\n"+
"	xhttp.send();\n"+
"}\n"+
"function querySetArticle(article) {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4)\n"+
"			if (this.status == 202) {\n"+
"				var article = JSON.parse(this.responseText);\n"+
"				updateArticle(article);\n"+
"				knowledge.article = article;\n"+
"				document.getElementById(\"showArticle\").style.display = \"block\";\n"+
"				document.getElementById(\"setArticle\").style.display = \"none\";\n"+
"			} else if (this.status == 409) {\n"+
"				alert(JSON.parse(this.responseText).error);\n"+
"			}\n"+
"	};\n"+
"	xhttp.open(\"PUT\", \"article/\"+article.id, true);\n"+
"	xhttp.setRequestHeader(\"Content-type\", \"application/json\");\n"+
"	xhttp.send(\"{\\\"name\\\":\\\"\"+article.name+\"\\\", \\\"reference\\\":\\\"\"+article.reference+\"\\\", \\\"quantity\\\":\"+article.quantity+\"}\");\n"+
"}\n"+
"function queryAddArticle(article) {\n"+
"	var xhttp = new XMLHttpRequest();\n"+
"	xhttp.onreadystatechange = function() {\n"+
"		if (this.readyState == 4)\n"+
"			if (this.status == 201) {\n"+
"				var article = JSON.parse(this.responseText);\n"+
"				updateArticle(article);\n"+
"				knowledge.article = article;\n"+
"				document.getElementById(\"showArticle\").style.display = \"block\";\n"+
"				document.getElementById(\"addArticle\").style.display = \"none\";\n"+
"			} else if (this.status == 409) {\n"+
"				alert(JSON.parse(this.responseText).error);\n"+
"			}\n"+
"	};\n"+
"	xhttp.open(\"POST\", \"shelf/\"+knowledge.shelf.id+\"/articles\", true);\n"+
"	xhttp.setRequestHeader(\"Content-type\", \"application/json\");\n"+
"	xhttp.send(\"{\\\"name\\\":\\\"\"+article.name+\"\\\", \\\"reference\\\":\\\"\"+article.reference+\"\\\", \\\"quantity\\\":\"+article.quantity+\"}\");\n"+
"}\n"+
"function tryAddMagasin() {\n"+
"	queryAddMagasin({name: document.getElementById(\"faddMagasinName\").value,\n"+
"		city: document.getElementById(\"faddMagasinCity\").value});\n"+
"}\n"+
"function trySetMagasin() {\n"+
"	querySetMagasin({id: knowledge.magasin.id,\n"+
"		name: document.getElementById(\"fsetMagasinName\").value,\n"+
"		city: document.getElementById(\"fsetMagasinCity\").value});\n"+
"}\n"+
"function tryAddShelf() {\n"+
"	queryAddShelf({name: document.getElementById(\"faddShelfName\").value});\n"+
"}\n"+
"function trySetShelf() {\n"+
"	querySetShelf({id: knowledge.shelf.id,\n"+
"		name: document.getElementById(\"fsetShelfName\").value});\n"+
"}\n"+
"function tryAddArticle() {\n"+
"	queryAddArticle({name: document.getElementById(\"faddArticleName\").value,\n"+
"		reference: document.getElementById(\"faddArticleReference\").value,\n"+
"		quantity: document.getElementById(\"faddArticleQuantity\").value});\n"+
"}\n"+
"function trySetArticle() {\n"+
"	querySetArticle({id: knowledge.article.id,\n"+
"		name: document.getElementById(\"fsetArticleName\").value,\n"+
"		reference: document.getElementById(\"fsetArticleReference\").value,\n"+
"		quantity: document.getElementById(\"fsetArticleQuantity\").value});\n"+
"}\n"+
"addEvent(document.getElementById(\"bListMagasins\"), \"click\", function() {\n"+
"	queryListOfMagasins();\n"+
"	var sections = [\"showMagasin\", \"addMagasin\", \"setMagasin\", \"showArticle\", \"addArticle\", \"setArticle\", \"showShelf\", \"addShelf\", \"setShelf\"];\n"+
"	for (var i = 0, l = sections.length; i < l; i++)\n"+
"		document.getElementById(sections[i]).style.display = \"none\";\n"+
"	document.getElementById(\"listMagasins\").style.display = \"block\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bAddMagasin\"), \"click\", function() {\n"+
"	document.getElementById(\"faddMagasinCity\").value = \"\";\n"+
"	document.getElementById(\"faddMagasinName\").value = \"\";\n"+
"	document.getElementById(\"addMagasin\").style.display = \"block\";\n"+
"	document.getElementById(\"listMagasins\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bokaddMagasin\"), \"click\", function() {\n"+
"	tryAddMagasin();\n"+
"});\n"+
"addEvent(document.getElementById(\"bcanceladdMagasin\"), \"click\", function() {\n"+
"	document.getElementById(\"listMagasins\").style.display = \"block\";\n"+
"	document.getElementById(\"addMagasin\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bAddShelf\"), \"click\", function() {\n"+
"	document.getElementById(\"faddShelfName\").value = \"\";\n"+
"	document.getElementById(\"addShelf\").style.display = \"block\";\n"+
"	document.getElementById(\"showMagasin\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bsetMagasin\"), \"click\", function() {\n"+
"	document.getElementById(\"fsetMagasinCity\").value = document.getElementById(\"showMagasinDisplayCity\").innerHTML;\n"+
"	document.getElementById(\"fsetMagasinName\").value = document.getElementById(\"showMagasinDisplayName\").innerHTML;\n"+
"	document.getElementById(\"setMagasin\").style.display = \"block\";\n"+
"	document.getElementById(\"showMagasin\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bdeleteMagasin\"), \"click\", function() {\n"+
"	if (confirm(\"Êtes-vous sûr de vouloir supprimer ce magasin ?\")) {\n"+
"		queryDeleteMagasin();\n"+
"		queryListOfMagasins();\n"+
"		document.getElementById(\"listMagasins\").style.display = \"block\";\n"+
"		document.getElementById(\"showMagasin\").style.display = \"none\";\n"+
"	}\n"+
"});\n"+
"addEvent(document.getElementById(\"boksetMagasin\"), \"click\", function() {\n"+
"	trySetMagasin();\n"+
"});\n"+
"addEvent(document.getElementById(\"bcancelsetMagasin\"), \"click\", function() {\n"+
"	document.getElementById(\"showMagasin\").style.display = \"block\";\n"+
"	document.getElementById(\"setMagasin\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bokaddShelf\"), \"click\", function() {\n"+
"	tryAddShelf();\n"+
"});\n"+
"addEvent(document.getElementById(\"bcanceladdShelf\"), \"click\", function() {\n"+
"	document.getElementById(\"showMagasin\").style.display = \"block\";\n"+
"	document.getElementById(\"addShelf\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bAddArticle\"), \"click\", function() {\n"+
"	document.getElementById(\"faddArticleName\").value = \"\";\n"+
"	document.getElementById(\"faddArticleReference\").value = \"\";\n"+
"	document.getElementById(\"faddArticleQuantity\").value = \"\";\n"+
"	document.getElementById(\"addArticle\").style.display = \"block\";\n"+
"	document.getElementById(\"showShelf\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bsetShelf\"), \"click\", function() {\n"+
"	document.getElementById(\"fsetShelfName\").value = document.getElementById(\"showShelfDisplayName\").innerHTML;\n"+
"	document.getElementById(\"setShelf\").style.display = \"block\";\n"+
"	document.getElementById(\"showShelf\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bdeleteShelf\"), \"click\", function() {\n"+
"	if (confirm(\"Êtes-vous sûr de vouloir supprimer ce rayon ?\")) {\n"+
"		queryDeleteShelf();\n"+
"		queryListOfShelves();\n"+
"		document.getElementById(\"showMagasin\").style.display = \"block\";\n"+
"		document.getElementById(\"showShelf\").style.display = \"none\";\n"+
"	}\n"+
"});\n"+
"addEvent(document.getElementById(\"boksetShelf\"), \"click\", function() {\n"+
"	trySetShelf();\n"+
"});\n"+
"addEvent(document.getElementById(\"bcancelsetShelf\"), \"click\", function() {\n"+
"	document.getElementById(\"showShelf\").style.display = \"block\";\n"+
"	document.getElementById(\"setShelf\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bokaddArticle\"), \"click\", function() {\n"+
"	tryAddArticle();\n"+
"});\n"+
"addEvent(document.getElementById(\"bcanceladdArticle\"), \"click\", function() {\n"+
"	document.getElementById(\"showShelf\").style.display = \"block\";\n"+
"	document.getElementById(\"addArticle\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bsetArticle\"), \"click\", function() {\n"+
"	document.getElementById(\"fsetArticleName\").value = document.getElementById(\"showArticleDisplayName\").innerHTML;\n"+
"	document.getElementById(\"fsetArticleReference\").value = document.getElementById(\"showArticleDisplayReference\").innerHTML;\n"+
"	document.getElementById(\"fsetArticleQuantity\").value = document.getElementById(\"showArticleDisplayQuantity\").innerHTML;\n"+
"	document.getElementById(\"setArticle\").style.display = \"block\";\n"+
"	document.getElementById(\"showArticle\").style.display = \"none\";\n"+
"});\n"+
"addEvent(document.getElementById(\"bdeleteArticle\"), \"click\", function() {\n"+
"	if (confirm(\"Êtes-vous sûr de vouloir supprimer cet article ?\")) {\n"+
"		queryDeleteArticle();\n"+
"		queryListOfArticles();\n"+
"		document.getElementById(\"showShelf\").style.display = \"block\";\n"+
"		document.getElementById(\"showArticle\").style.display = \"none\";\n"+
"	}\n"+
"});\n"+
"addEvent(document.getElementById(\"boksetArticle\"), \"click\", function() {\n"+
"	trySetArticle();\n"+
"});\n"+
"addEvent(document.getElementById(\"bcancelsetArticle\"), \"click\", function() {\n"+
"	document.getElementById(\"showArticle\").style.display = \"block\";\n"+
"	document.getElementById(\"setArticle\").style.display = \"none\";\n"+
"});\n"+
"queryListOfMagasins();\n"+
"var sections = [\"showMagasin\", \"addMagasin\", \"setMagasin\", \"showArticle\", \"addArticle\", \"setArticle\", \"showShelf\", \"addShelf\", \"setShelf\"];\n"+
"for (var i = 0, l = sections.length; i < l; i++)\n"+
"	document.getElementById(sections[i]).style.display = \"none\";\n"+
"document.getElementById(\"listMagasins\").style.display = \"block\";";

}
