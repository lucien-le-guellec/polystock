package condtest.restapi.controller.impl;

import condtest.restapi.assembler.ArticleResourceAssembler;
import condtest.restapi.entity.Article;
import condtest.restapi.service.ArticleService;
import condtest.restapi.exception.EntityNotFoundException;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "/article/")
public class ArticleControllerImpl {

	static final private String FORMAT_ID_NOT_FOUND = "{\"error\":\"Entity with id [%d] not found\"}";
	static final private String FORMAT_ARTICLE = "{\"id\":%d,\"name\":\"%s\",\"reference\":\"%s\",\"quantity\":%d}";

	private final ArticleService entityService;

	private final ArticleResourceAssembler resourceAssembler;

	public ArticleControllerImpl(final ArticleService mS, final ArticleResourceAssembler rA) {
		this.resourceAssembler = rA;
		this.entityService = mS;
	}

	private ResponseEntity<String> idNotFound(Long id) {
		return new ResponseEntity<String>(String.format(FORMAT_ID_NOT_FOUND, id), HttpStatus.NOT_FOUND);
	}

	private ResponseEntity<String> returnArticle(Article m) {
		return returnArticle(m, HttpStatus.OK);
	}

	private ResponseEntity<String> returnArticle(Article m, HttpStatus status) {
		return new ResponseEntity<String>(String.format(FORMAT_ARTICLE, m.getId(), m.getName(), m.getReference(), m.getQuantity()), status);
	}

	@GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> one(@PathVariable final Long id) {
		Optional<Article> o = this.entityService.findById(id);
		if (!o.isPresent())
			return this.idNotFound(id);
		return returnArticle(o.get(), HttpStatus.OK);
	}

	@PutMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> update(@PathVariable final Long id, @RequestBody final Article entity) throws URISyntaxException {
		Article m;
		try {
			m = this.entityService.update(id, entity);
		} catch(EntityNotFoundException e) {
			return this.idNotFound(id);
		}
		return returnArticle(m, HttpStatus.ACCEPTED);
	}

	@DeleteMapping(path = "{id}")
	public ResponseEntity<String> delete(@PathVariable final Long id) {
		try {
			this.entityService.delete(id);
		} catch(EntityNotFoundException e) {
			return idNotFound(id);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}

