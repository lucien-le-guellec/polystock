package condtest.restapi.repository;

import java.lang.Iterable;

import condtest.restapi.entity.Magasin;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MagasinRepository extends CrudRepository<Magasin, Long> {

	Iterable<Magasin> findByName(String name);

}
