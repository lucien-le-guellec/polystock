package condtest.restapi.repository;

import java.lang.Iterable;

import condtest.restapi.entity.Shelf;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShelfRepository extends CrudRepository<Shelf, Long> {

	Iterable<Shelf> findByName(String name);

}
