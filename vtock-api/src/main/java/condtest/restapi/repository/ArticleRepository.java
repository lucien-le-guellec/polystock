package condtest.restapi.repository;

import java.lang.Iterable;

import condtest.restapi.entity.Article;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends CrudRepository<Article, Long> {

}
