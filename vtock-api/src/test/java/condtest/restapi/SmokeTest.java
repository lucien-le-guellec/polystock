package condtest.restapi;

import condtest.restapi.controller.impl.MagasinControllerImpl;
import condtest.restapi.controller.impl.ShelfControllerImpl;
import condtest.restapi.controller.impl.ArticleControllerImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmokeTest {

	@Autowired
	private MagasinControllerImpl magasinControllerImpl;

	@Autowired
	private ShelfControllerImpl shelfControllerImpl;

	@Autowired
	private ArticleControllerImpl articleControllerImpl;

	@Test
	public void contextLoads() {
		Assert.assertNotNull(magasinControllerImpl);
		Assert.assertNotNull(shelfControllerImpl);
		Assert.assertNotNull(articleControllerImpl);
	}
}
