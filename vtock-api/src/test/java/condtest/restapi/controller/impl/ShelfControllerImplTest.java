package condtest.restapi.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import condtest.restapi.entity.Shelf;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ShelfControllerImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String ID_666_NOT_FOUND = "Entity with id [666] not found";
	private static final String NAME_shelf11_ALREADY_TAKEN = "Entity with name [shelf11] already exists";

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetShelfAlone() throws Exception {
		this.mockMvc.perform(get("/shelf/1")).andDo(print()).andExpect(status().isOk())
			.andExpect(jsonPath("id", is(1)))
			.andExpect(jsonPath("name", is("shelf11")));
	}

	@Test
	public void testGetShelf() throws Exception {
		this.mockMvc.perform(get("/shelf/3")).andDo(print()).andExpect(status().isOk())
			.andExpect(jsonPath("id", is(3)))
			.andExpect(jsonPath("name", is("shelf32")));
	}

	@Test
	public void testGetUnknownShelf() throws Exception {
		this.mockMvc.perform(get("/shelf/666")).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_666_NOT_FOUND)));
	}

	@Test
	@DirtiesContext
	public void testPut() throws Exception {
		final Shelf m = new Shelf();
		m.setName("Spaaaaace");
		final MvcResult mvcResult =
			this.mockMvc.perform(put("/shelf/1")
				.content(OBJECT_MAPPER.writeValueAsString(m))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.ALL))
			.andDo(print()).andExpect(status().isAccepted())
			.andExpect(jsonPath("id", is(1)))
			.andExpect(jsonPath("name", is("Spaaaaace")))
			.andReturn();
	}

	@Test
	public void testDeleteEntityNotFound() throws Exception {
		this.mockMvc.perform(delete("/shelf/666")).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_666_NOT_FOUND)));
	}

	@Test
	@DirtiesContext
	public void testDelete() throws Exception {
		this.mockMvc.perform(delete("/shelf/3")).andDo(print()).andExpect(status().isNoContent());
	}
}
