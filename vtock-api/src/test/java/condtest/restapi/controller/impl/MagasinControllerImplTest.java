package condtest.restapi.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import condtest.restapi.entity.Magasin;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MagasinControllerImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String ID_666_NOT_FOUND = "Entity with id [666] not found";
	private static final String ID_1_NOT_FOUND = "Entity with id [1] not found";
	private static final String NAME_magasin1_ALREADY_TAKEN = "Entity with name [magasin1] already exists";

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetMagasinAlone() throws Exception {
		this.mockMvc.perform(get("/magasin/1")).andDo(print()).andExpect(status().isOk())
			.andExpect(jsonPath("id", is(1)))
			.andExpect(jsonPath("name", is("magasin1")))
			.andExpect(jsonPath("city", is("Nuremberg")));
	}

	@Test
	public void testGetMagasin() throws Exception {
		this.mockMvc.perform(get("/magasin/2")).andDo(print()).andExpect(status().isOk())
			.andExpect(jsonPath("id", is(2)))
			.andExpect(jsonPath("name", is("magasin2")))
			.andExpect(jsonPath("city", is("Tokyo")));
	}

	@Test
	public void testGetUnknownMagasin() throws Exception {
		this.mockMvc.perform(get("/magasin/666")).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_666_NOT_FOUND)));
	}

	@Test
	public void testGetAllMagasins() throws Exception {
		this.mockMvc.perform(get("/magasin/")).andDo(print()).andExpect(status().isOk())
			.andExpect(jsonPath("_embedded.magasinList", hasSize(3)))
			.andExpect(jsonPath("_embedded.magasinList[0].id", is(1)))
			.andExpect(jsonPath("_embedded.magasinList[1].id", is(2)))
			.andExpect(jsonPath("_embedded.magasinList[2].id", is(3)))
			.andExpect(jsonPath("_embedded.magasinList[0].name", is("magasin1")))
			.andExpect(jsonPath("_embedded.magasinList[1].name", is("magasin2")))
			.andExpect(jsonPath("_embedded.magasinList[2].name", is("magasin3")))
			.andExpect(jsonPath("_embedded.magasinList[0].city", is("Nuremberg")))
			.andExpect(jsonPath("_embedded.magasinList[1].city", is("Tokyo")))
			.andExpect(jsonPath("_embedded.magasinList[2].city", is("Freetown")));
	}

	@Test
	public void testPostEntityAlreadyExists() throws Exception {
		final Magasin m = new Magasin();
		m.setName("magasin1");
		m.setCity("?!$*");
		this.mockMvc.perform(post("/magasin/").content(OBJECT_MAPPER.writeValueAsString(m)).contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.ALL)).andDo(print()).andExpect(status().isConflict())
			.andExpect(jsonPath("error", is(NAME_magasin1_ALREADY_TAKEN)));
	}

	@Test
	@DirtiesContext
	public void testPost() throws Exception {
		final Magasin m = new Magasin();
		m.setName("magasin666");
		m.setCity("Stalingrad");
		final MvcResult mvcResult = this.mockMvc.perform(post("/magasin/").content(OBJECT_MAPPER.writeValueAsString(m)).contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.ALL)).andDo(print()).andExpect(status().isCreated())
			.andExpect(jsonPath("name", is("magasin666")))
			.andExpect(jsonPath("city", is("Stalingrad")))
			.andReturn();
	}

	@Test
	@DirtiesContext
	public void testPut() throws Exception {
		final Magasin m = new Magasin();
		m.setName("magasin666");
		m.setCity("THE_TOUCAN_HAS_ARRIVED");
		final MvcResult mvcResult =
			this.mockMvc.perform(put("/magasin/1")
				.content(OBJECT_MAPPER.writeValueAsString(m))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.ALL))
			.andDo(print()).andExpect(status().isAccepted())
			.andExpect(jsonPath("id", is(1)))
			.andExpect(jsonPath("name", is("magasin666")))
			.andExpect(jsonPath("city", is("THE_TOUCAN_HAS_ARRIVED")))
			.andReturn();
	}

	@Test
	public void testDeleteEntityNotFound() throws Exception {
		this.mockMvc.perform(delete("/magasin/666")).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_666_NOT_FOUND)));
	}

	@Test
	@DirtiesContext
	public void testDeleteEmptyMagasin() throws Exception {
		this.mockMvc.perform(delete("/magasin/3")).andDo(print()).andExpect(status().isNoContent());
	}

	@Test
	@DirtiesContext
	public void testDeleteMagasinWithShelves() throws Exception {
		this.mockMvc.perform(delete("/magasin/1")).andDo(print()).andExpect(status().isNoContent());
		this.mockMvc.perform(get("/magasin/1")).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_1_NOT_FOUND)));
	}
}
