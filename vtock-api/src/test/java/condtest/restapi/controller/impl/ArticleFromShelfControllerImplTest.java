package condtest.restapi.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import condtest.restapi.entity.Article;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ArticleFromShelfControllerImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String ID_666_NOT_FOUND = "Entity with id [666] not found";

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetNoArticles() throws Exception {
		this.mockMvc.perform(get("/shelf/1/articles"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string("{}"));
	}

	@Test
	public void testGetNoArticlesFromUnknownShelf() throws Exception {
		this.mockMvc.perform(get("/shelf/666/articles"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string("{}"));
	}

	@Test
	public void testGetArticles() throws Exception {
		this.mockMvc.perform(get("/shelf/2/articles"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("_embedded.articleList[0].id", is(1)))
			.andExpect(jsonPath("_embedded.articleList[1].id", is(2)))
			.andExpect(jsonPath("_embedded.articleList[2].id", is(3)))
			.andExpect(jsonPath("_embedded.articleList[0].name", is("article12")))
			.andExpect(jsonPath("_embedded.articleList[1].name", is("article22")))
			.andExpect(jsonPath("_embedded.articleList[2].name", is("article32")))
			.andExpect(jsonPath("_embedded.articleList[0].reference", is("ytr")))
			.andExpect(jsonPath("_embedded.articleList[1].reference", is("aze")))
			.andExpect(jsonPath("_embedded.articleList[2].reference", is("oiu")))
			.andExpect(jsonPath("_embedded.articleList[0].quantity", is(1789)))
			.andExpect(jsonPath("_embedded.articleList[1].quantity", is(1936)))
			.andExpect(jsonPath("_embedded.articleList[2].quantity", is(1917)));
	}

	@Test
	@DirtiesContext
	public void testPostArticle() throws Exception {
		final Article m = new Article();
		m.setName("Spaaaaace");
		m.setReference("spacedoesntexist");
		m.setQuantity(1515);
		final MvcResult mvcResult = this.mockMvc.perform(post("/shelf/1/articles/").content(OBJECT_MAPPER.writeValueAsString(m)).contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.ALL)).andDo(print()).andExpect(status().isCreated())
			.andExpect(jsonPath("name", is("Spaaaaace")))
			.andExpect(jsonPath("reference", is("spacedoesntexist")))
			.andExpect(jsonPath("quantity", is(1515)))
			.andReturn();
	}

	@Test
	public void testPostArticleOfUnknownShelf() throws Exception {
		final Article m = new Article();
		m.setName("Spaaaaace");
		m.setReference("spacedoesntexist");
		m.setQuantity(1515);
		this.mockMvc.perform(post("/shelf/666/acticles/").content(OBJECT_MAPPER.writeValueAsString(m)).contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.ALL)).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_666_NOT_FOUND)));
	}
}
