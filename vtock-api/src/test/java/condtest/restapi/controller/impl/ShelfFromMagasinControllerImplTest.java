package condtest.restapi.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import condtest.restapi.entity.Shelf;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ShelfFromMagasinControllerImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String ID_666_NOT_FOUND = "Entity with id [666] not found";
	private static final String NAME_shelf11_ALREADY_TAKEN = "Entity with name [shelf11] already exists";

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetNoShelves() throws Exception {
		this.mockMvc.perform(get("/magasin/3/shelves"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string("{}"));
	}

	@Test
	public void testGetNoShelvesFromUnknownMagasin() throws Exception {
		this.mockMvc.perform(get("/magasin/666/shelves"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().string("{}"));
	}

	@Test
	public void testGetShelves() throws Exception {
		this.mockMvc.perform(get("/magasin/1/shelves"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("_embedded.shelfList[0].id", is(1)))
			.andExpect(jsonPath("_embedded.shelfList[1].id", is(2)))
			.andExpect(jsonPath("_embedded.shelfList[0].name", is("shelf11")))
			.andExpect(jsonPath("_embedded.shelfList[1].name", is("shelf21")));
	}

	@Test
	public void testPostShelfAlreadyExists() throws Exception {
		final Shelf s = new Shelf();
		s.setName("shelf11");
		this.mockMvc.perform(post("/magasin/1/shelves/").content(OBJECT_MAPPER.writeValueAsString(s)).contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.ALL)).andDo(print()).andExpect(status().isConflict())
			.andExpect(jsonPath("error", is(NAME_shelf11_ALREADY_TAKEN)));
	}

	@Test
	@DirtiesContext
	public void testPostShelf() throws Exception {
		final Shelf m = new Shelf();
		m.setName("Spaaaaace");
		final MvcResult mvcResult = this.mockMvc.perform(post("/magasin/1/shelves/").content(OBJECT_MAPPER.writeValueAsString(m)).contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.ALL)).andDo(print()).andExpect(status().isCreated())
			.andExpect(jsonPath("name", is("Spaaaaace")))
			.andReturn();
	}

	@Test
	public void testPostShelfOfUnknownMagasin() throws Exception {
		final Shelf m = new Shelf();
		m.setName("Spaaaaace");
		this.mockMvc.perform(post("/magasin/666/shelves/").content(OBJECT_MAPPER.writeValueAsString(m)).contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.ALL)).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_666_NOT_FOUND)));
	}

}
