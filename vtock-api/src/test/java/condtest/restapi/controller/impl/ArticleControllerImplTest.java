package condtest.restapi.controller.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import condtest.restapi.entity.Article;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ArticleControllerImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String ID_666_NOT_FOUND = "Entity with id [666] not found";

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testGetArticleAlone() throws Exception {
		this.mockMvc.perform(get("/article/1")).andDo(print()).andExpect(status().isOk())
			.andExpect(jsonPath("id", is(1)))
			.andExpect(jsonPath("name", is("article12")))
			.andExpect(jsonPath("reference", is("ytr")))
			.andExpect(jsonPath("quantity", is(1789)));
	}

	@Test
	public void testGetArticle() throws Exception {
		this.mockMvc.perform(get("/article/2")).andDo(print()).andExpect(status().isOk())
			.andExpect(jsonPath("id", is(2)))
			.andExpect(jsonPath("name", is("article22")))
			.andExpect(jsonPath("reference", is("aze")))
			.andExpect(jsonPath("quantity", is(1936)));
	}

	@Test
	public void testGetUnknownArticle() throws Exception {
		this.mockMvc.perform(get("/article/666")).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_666_NOT_FOUND)));
	}

	@Test
	@DirtiesContext
	public void testPut() throws Exception {
		final Article m = new Article();
		m.setName("Spaaaaace");
		m.setReference("spacedoesntexist");
		m.setQuantity(1515);
		final MvcResult mvcResult = this.mockMvc.perform(put("/article/1").content(OBJECT_MAPPER.writeValueAsString(m)).contentType(MediaType.APPLICATION_JSON)
			.accept(MediaType.ALL)).andDo(print()).andExpect(status().isAccepted())
			.andExpect(jsonPath("id", is(1)))
			.andExpect(jsonPath("name", is("Spaaaaace")))
			.andExpect(jsonPath("reference", is("spacedoesntexist")))
			.andExpect(jsonPath("quantity", is(1515)))
			.andReturn();
	}

	@Test
	public void testDeleteEntityNotFound() throws Exception {
		this.mockMvc.perform(delete("/article/666")).andDo(print()).andExpect(status().isNotFound())
			.andExpect(jsonPath("error", is(ID_666_NOT_FOUND)));
	}

	@Test
	@DirtiesContext
	public void testDelete() throws Exception {
		this.mockMvc.perform(delete("/article/3")).andDo(print()).andExpect(status().isNoContent());
	}
}
