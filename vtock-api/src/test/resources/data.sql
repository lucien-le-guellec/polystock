insert into magasin(id, name, city) values (1, 'magasin1', 'Nuremberg');
insert into magasin(id, name, city) values (2, 'magasin2', 'Tokyo');
insert into magasin(id, name, city) values (3, 'magasin3', 'Freetown');

insert into shelf(id, name, magasin_id) values(1, 'shelf11', 1);
insert into shelf(id, name, magasin_id) values(2, 'shelf21', 1);
insert into shelf(id, name, magasin_id) values(3, 'shelf32', 2);

insert into article(id, name, reference, quantity, shelf_id) values(1, 'article12', 'ytr', 1789, 2);
insert into article(id, name, reference, quantity, shelf_id) values(2, 'article22', 'aze', 1936, 2);
insert into article(id, name, reference, quantity, shelf_id) values(3, 'article32', 'oiu', 1917, 2);
insert into article(id, name, reference, quantity, shelf_id) values(4, 'article43', 'qsd', 1793, 3);
